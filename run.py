#!/usr/bin/python3
import os
import json
import argparse
import subprocess
from zipfile import ZipFile

# Constants / Globals
PATH_DIR = os.path.dirname(os.path.abspath(__file__)) + "/"
STUDENT_ID_FILE = ".student_id"
g_student = {"id":None, "name":None, "surname":None}

PATHS = {
  'test'  : {
    'lab1' : [
      'libs/rtu/components/half_adder/sim/run.py',
      'libs/rtu/components/full_adder/sim/run.py',
      'libs/rtu/components/ripple_carry_adder/sim/run.py',
      'libs/rtu/components/clock_divider/sim/run.py'
    ],
    'lab2' : [
      'libs/rtu/components/counter/sim/run.py',
      'libs/rtu/components/sync_generator/sim/run.py',
    ],
    'lab3' : [
      'libs/rtu/components/counter/sim/run.py',
      'libs/rtu/components/sipo_register/sim/run.py',
      'libs/rtu/components/piso_register/sim/run.py',
      'libs/rtu/components/uart_rx_slave/sim/run.py',
      'libs/rtu/components/uart_tx_master/sim/run.py',
      'libs/rtu/components/edge_detector/sim/run.py',
      'libs/rtu/components/button_debounce/sim/run.py',
      'lab3/de10_standard/src/uart_command_generator/sim/run.py',
      'lab3/de10_standard/src/uart_command_parser/sim/run.py',
    ],
    'lab4' : [
      'libs/rtu/components/sipo_register/sim/run.py',
      'libs/rtu/components/piso_register/sim/run.py',
      'libs/rtu/components/spi_master/sim/run.py',
    ],
    'ps1' : [
      'ps1/challenge_5/sim/run.py',
      'ps1/challenge_6/sim/run.py'
    ]
  },
  'patch' : {
    'lab1' : [
      'libs/rtu/pkg/data_types.vhd',
      'libs/rtu/pkg/procedures.vhd',
      'libs/rtu/pkg/functions.vhd',
      'libs/rtu/components/half_adder/src/half_adder.vhd',
      'libs/rtu/components/full_adder/src/full_adder.vhd',
      'libs/rtu/components/ripple_carry_adder/src/ripple_carry_adder.vhd',
      'libs/rtu/components/clock_divider/src/clock_divider.vhd',
    ],
    'lab2' : [
      'libs/rtu/pkg/data_types.vhd',
      'libs/rtu/pkg/procedures.vhd',
      'libs/rtu/pkg/functions.vhd',
      'libs/rtu/components/counter/src/counter.vhd',
      'libs/rtu/components/sync_generator/src/sync_generator.vhd',
      'lab2/de0_nano_soc/src/pattern_from_coords.vhd',
      'lab2/de0_nano_soc/src/pattern_from_memory.vhd',
      'lab2/de10_standard/src/pattern_from_coords.vhd',
      'lab2/de10_standard/src/pattern_from_memory.vhd',
    ],
    'lab3' : [
      'libs/rtu/pkg/data_types.vhd',
      'libs/rtu/pkg/procedures.vhd',
      'libs/rtu/pkg/functions.vhd',
      'libs/rtu/components/counter/src/counter.vhd',
      'libs/rtu/components/sipo_register/src/sipo_register.vhd',
      'libs/rtu/components/piso_register/src/piso_register.vhd',
      'libs/rtu/components/uart_rx_slave/src/uart_rx_slave.vhd',
      'libs/rtu/components/uart_tx_master/src/uart_tx_master.vhd',
      'libs/rtu/components/edge_detector/src/edge_detector.vhd',
      'libs/rtu/components/button_debounce/src/button_debounce.vhd',
      'lab3/de10_standard/src/uart_command_generator/src/uart_command_generator.vhd',
      'lab3/de10_standard/src/uart_command_parser/src/uart_command_parser.vhd',
    ],
    'lab4' : [
      'libs/rtu/pkg/data_types.vhd',
      'libs/rtu/pkg/procedures.vhd',
      'libs/rtu/pkg/functions.vhd',
      'libs/rtu/components/sipo_register/src/sipo_register.vhd',
      'libs/rtu/components/piso_register/src/piso_register.vhd',
      'libs/rtu/components/spi_master/src/spi_master.vhd',
      'libs/rtu/components/lis3dh_master/src/lis3dh_master.vhd',
    ],
    'ps1' : [
      'ps1/challenge_5/src/circular_shift_left.vhd',
      'ps1/challenge_6/src/leading_one_position_locater.vhd'
    ]
  }
}


def check_first_usage(args):
  global g_student

  # check if student if file exists
  if not os.path.isfile(STUDENT_ID_FILE):
    print('Missing student information detected')
    g_student['id']      = input('Please provide your student ID: ')
    g_student['name']    = input('Please provide your name: ')
    g_student['surname'] = input('Please provide your surname: ')

    print('Storing information in "' + STUDENT_ID_FILE + '" file...')
    json.dump(g_student, open(STUDENT_ID_FILE, 'w'))
    print('Modify or delete "' + STUDENT_ID_FILE + '" file to update information')

  else:
    g_student = json.load(open(STUDENT_ID_FILE))


def run_test(args):
  # run all tests
  if args.target == 'all':
    for target in PATHS[args.action]:
      for path in PATHS[args.action][target]:
        subprocess.run('cd ' + os.path.dirname(os.path.abspath(PATH_DIR + path)) + ' && python3 ' + PATH_DIR + path, shell=True)
  # run specific test
  else:
    for path in PATHS[args.action][args.target]:
      subprocess.run('cd ' + os.path.dirname(os.path.abspath(PATH_DIR + path)) + ' && python3 ' + PATH_DIR + path, shell=True)


def run_patch(args):
  fname = g_student['id'].lower() + '_' + \
          g_student['name'].lower() + '_' + \
          g_student['surname'].lower() + '_' + \
          args.target + '.zip'

  zipObj = ZipFile(fname, 'w')
  for i in PATHS[args.action][args.target]:
    print('Patching file: "' + i + '"...')
    zipObj.write(i)

  print('Files patched into "' + fname + '" file');

if __name__ == '__main__':
  # setup arguments
  parser = argparse.ArgumentParser()
  subparsers = parser.add_subparsers(dest='action', help='Select action')

  parser_test  = subparsers.add_parser('test',
    help='Run RTL simulations for the labs and problem sets')
  parser_test.add_argument('target',
    choices=['all','ps1','lab1','lab2','lab3', 'lab4'])

  parser_patch = subparsers.add_parser('patch',
    help='Patch design files for the specific lab / problem set')
  parser_patch.add_argument('target',
    choices=['ps1','lab1','lab2','lab3', 'lab4'])

  args = parser.parse_args()

  check_first_usage(args)

  if args.action == 'test':
    run_test(args)
  elif args.action == 'patch':
    run_patch(args)
  else:
    parser.print_help()
