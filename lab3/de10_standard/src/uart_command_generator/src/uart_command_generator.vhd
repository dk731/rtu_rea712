library ieee;
library rtu;
library lab3;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use rtu.functions.all;
use lab3.functions.all;

-- @brief Component continiously generates UART commands for achieving
entity uart_command_generator is
  port(
    clk     : in  std_logic;

    -- user input
    i_btn0_rising_event  : in  std_logic;
    i_btn0_falling_event : in  std_logic;
    i_btn1_rising_event  : in  std_logic;
    i_btn1_falling_event : in  std_logic;

    -- uart master
    i_busy  : in  std_logic; 
    o_valid : out std_logic;
    o_data  : out std_logic_vector(7 downto 0)
  );
end entity;


architecture RTL of uart_command_generator is
  constant COUNTER_MAX_VALUE : natural := 255;

  -- high-level state machine
  type state_hl_t is (s_idle, s_btn0_prepare, s_sending_leds, s_sending_hex0, s_sending_hex1);
  -- low-level state machine
  type state_ll_t is (s_idle, s_sending_cmd, s_sending_addr, s_sending_data);

  signal state_hl_reg, state_hl_next : state_hl_t := s_idle;
  signal state_ll_reg, state_ll_next : state_ll_t := s_idle;
  signal counter_value : unsigned(7 downto 0) := (others => '0');
  signal counter_en    : std_logic := '0';
  signal valid_reg, valid_next : std_logic := '0';
  signal data_reg,  data_next : std_logic_vector(o_data'range) := (others => '0');

  -- intermediate command address and data storage
  signal wreq_asyn  : std_logic := '0';
  signal waddr_reg, waddr_next : std_logic_vector(o_data'range) := (others => '0');
  signal wdata_reg, wdata_next : std_logic_vector(o_data'range) := (others => '0');
begin

  -- counter component instantation
  U0: entity rtu.counter
  generic map(COUNTER_MAX_VALUE => COUNTER_MAX_VALUE)
  port map(
    clk       => clk,
    en        => counter_en,
    o_counter => counter_value);


  -- reg-state logic
  process(clk)
  begin
    if rising_edge(clk) then
      state_hl_reg <= state_hl_next;
      state_ll_reg <= state_ll_next;
      waddr_reg    <= waddr_next;
      wdata_reg    <= wdata_next;
      valid_reg    <= valid_next;
      data_reg     <= data_next; 
    end if;
  end process;


  -- next-state logic (high-level FSM)
  process(all)
  begin
    -- default
    state_hl_next <= state_hl_reg;
    waddr_next    <= waddr_reg;
    wdata_next    <= wdata_reg;
    wreq_asyn     <= '0';
    counter_en    <= '0';

    case state_hl_reg is
      when s_idle =>
        -- by default stay in this state if no rising events have been received
        -- TODO: if rising_edge is detected for the btn0
        --   (1) enable (increment) counter
        --   (2) go to 's_btn0_prepare' state


      when s_btn0_prepare =>
        -- TODO:
        --   (1) prepare 'waddr_next' with LEDs address - 0x00
        --   (2) prepare 'wdata_next' with counter's value
        --   (3) set asyncronous write command ('wreq_asyn' signal)

        -- TODO: (4) if the low-level state machine is in idle state, switch
        --           to 's_sending_leds' state


      when s_sending_leds =>
        -- TODO: if the low-level state machine is in idle state it is safe to carry out
        -- the next action, i.e.:
        --   (1) prepare 'waddr_next' with HEX0 address - 0x01
        --   (2) prepare 'wdata_next' with counter's value using dec2segment routine (low bits)
        --   (3) set asyncronous write command ('wreq_asyn' signal)
        --   (4) switch to 's_sending_leds' state

      when s_sending_hex0 =>
        -- TODO: if the low-level state machine is in idle state it is safe to carry out
        -- the next action, i.e.:
        --   (1) prepare 'waddr_next' with HEX1 address - 0x02
        --   (2) prepare 'wdata_next' with counter's value using dec2segment routine (high bits)
        --   (3) set asyncronous write command ('wreq_asyn' signal)
        --   (4) switch to 's_sending_leds' state

      when s_sending_hex1 =>
        -- TODO: if the low-level state machine is in idle state return to the idle state

    end case;
  end process;


  -- next-state logic (low-level FSM)
  process(all)
  begin
    -- default
    state_ll_next <= state_ll_reg;
    valid_next    <= '0';
    data_next     <= (others => '0');

    case state_ll_reg is
      when s_idle =>
        -- TODO: if not busy and 'wreq_asyn' asserted
        --  (1) go to 's_sending_cmd' state
        --  (2) during state switching, initiate write command (valid and data) with 'W' character command

      when s_sending_cmd =>
        -- TODO: if not busy
        --  (1) go to 's_sending_addr' state
        --  (2) during state switching, initiate write command (valid and data) with the address

      when s_sending_addr =>
        -- TODO: if not busy
        --  (1) go to 's_sending_data' state
        --  (2) during state switching, initiate write command (valid and data) with the data 

      when s_sending_data =>
        -- TODO: if not busy, go to 's_idle' state

    end case;
  end process;


  -- outputs
  o_valid <= valid_reg;
  o_data  <= data_reg;
 
end architecture;

