library ieee;
library rtu;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use rtu.functions.all;

entity uart_command_parser is
  generic(
    REGISTER_ADDR_WIDTH : natural := 4;
    REGISTER_DATA_WIDTH : natural := 8
  );
  port(
    clk     : in  std_logic;

    -- from uart slave
    i_valid : in  std_logic;
    i_data  : in  std_logic_vector(7 downto 0);

    -- towards regmap (write port)
    o_wreq   : out std_logic;
    o_waddr  : out std_logic_vector(REGISTER_ADDR_WIDTH-1 downto 0);
    o_wdata  : out std_logic_vector(REGISTER_DATA_WIDTH-1 downto 0);
    o_error  : out std_logic
  );
end entity;


architecture RTL of uart_command_parser is
  type state_t is (s_idle, s_recv_wcmd, s_recv_waddr, s_recv_wdata, s_wrequest, s_error);
  signal state_reg, state_next : state_t := s_idle;
  signal waddr_reg, waddr_next : std_logic_vector(o_waddr'range) := (others => '0');
  signal wdata_reg, wdata_next : std_logic_vector(o_wdata'range) := (others => '0');
  signal waddr_en, wdata_en : std_logic;
  signal wreq_reg, wreq_next : std_logic := '0';
begin

  -- reg-state logic
  process(clk)
  begin
    if rising_edge(clk) then
      -- TODO: describe state register
      -- TODO: describe waddr register with enable
      -- TODO: describe wdata register with enable
      -- TODO: describe wreq register
    end if;
  end process;

  -- next-state logic (state machine)
  process(all)
  begin
    -- default
    state_next <= state_reg;
    o_error    <= '0';
    waddr_next <= i_data(waddr_next'range);
    wdata_next <= i_data(wdata_next'range);
    waddr_en   <= '0';
    wdata_en   <= '0';
    wreq_next  <= '0';

    case state_reg is
      when s_idle =>
        -- TODO: stay in this state if no 'i_valid' has been received
        -- TODO: stay in this state if reset (character 'R') command has been received
        -- TODO: go to 's_recv_wcmd' state if valid (character 'W') command has been received
        -- TODO: go to 's_error' state if the received command is invalid

      when s_recv_wcmd =>
        -- TODO: stay in this state if no 'i_valid' has been received
        -- TODO: otherwise store received data into waddr register and go to 's_recv_waddr' state

      when s_recv_waddr =>
        -- TODO: stay in this state if no 'i_valid' has been received
        -- TODO: otherwise store received data into wdata register and go to 's_recv_wdata' state

      when s_recv_wdata =>
        -- TODO: generate write request and go to 's_wrequest' state

      when s_wrequest =>
        -- TODO: go to 's_idle' state

      when s_error =>
        -- TODO: assert 'o_error'
        -- TODO: stay in this state until reset (character 'R') command has been received

    end case;
  end process;


  -- outputs
  o_wreq   <= wreq_reg;
  o_waddr  <= waddr_reg;
  o_wdata  <= wdata_reg;

end architecture;
