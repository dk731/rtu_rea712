## Part 1. Hello, world!

1. With the guidance of the lecturer, complete the following circuits in the repositories' "rtu" library (note the provided simulation testbenches):
     - *half_adder*;
     - *full_adder*;
     - *ripple_carry_adder*.

2. Describe a "Hello, World!" (blinking led) circuit in VHDL by using the provided pin configuration. **Note that the project template is already provided in the git repository.**

3. Use **Signal tap** digital logic analyzer tool to set up recording of the circuit's signals (counter, led). Take a snapshot of counter at reseting condition. The tool is available under: *Tools - Signal Tap Logic Analyzer*.
