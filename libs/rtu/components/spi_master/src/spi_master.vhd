library ieee;
library rtu;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use rtu.data_types.all;

entity spi_master is
  port(
    clk  : in std_logic;  --! clock
    rst  : in std_logic;  --! asynchronous reset

    -- master control interface
    i_start : in std_logic;                     --! start pulse (for 1 clock cycle)
    i_rw    : in std_logic;                     --! read/write 0 => write, 1 => read
    i_addr  : in std_logic_vector(5 downto 0);  --! register address
    i_data  : in std_logic_vector(7 downto 0);  --! write data
    o_done  : out std_logic;                    --! done pulse (for 1 clock cycle)
    o_data  : out std_logic_vector(7 downto 0); --! read data

    -- spi interface
    spi_cs  : out std_logic; --! spi interface, chip select
    spi_clk : out std_logic; --! spi interface, clock
    spi_sdi : out std_logic; --! spi interface, slave-data-in
    spi_sdo : in  std_logic  --! spi interface, slave-data-out
  );
end entity;

architecture RTL of spi_master is
  -- state definitions (hl - high level, ll - low level), defined in rtu, data_types.vhd package
  -- type state_ll_t is (s_idle, s_start, s_clk_low0, s_clk_low1, s_clk_high0, s_clk_high1, s_stop);
  -- type state_hl_t is (s_idle, s_command, s_write, s_read);

  signal state_hl_reg, state_hl_next : state_hl_t := s_idle;
  signal state_ll_reg, state_ll_next : state_ll_t := s_idle;
  signal counter_reg, counter_next : unsigned(2 downto 0) := (others => '0');
  signal done_reg, done_next : std_logic := '0';

  -- interfacing signals
  signal piso_load, piso_enable, piso_data_out : std_logic;
  signal piso_data_in : std_logic_vector(7 downto 0);
  signal sipo_enable, sipo_data_in : std_logic;
  signal sipo_data_out : std_logic_vector(7 downto 0);

  -- input registration
  signal rw_reg, rw_next : std_logic := '0';
  signal data_reg, data_next : std_logic_vector(i_data'range) := (others =>'0');

begin

  -- parallel-in-serial-out register
  U0: entity rtu.piso_register
  port map(
    clk      => clk,
    rst      => rst,
    i_load   => piso_load,
    i_enable => piso_enable,
    i_data   => piso_data_in,
    o_data   => piso_data_out
  );

  -- serial-in-parallel-out register
  U1: entity rtu.sipo_register
  port map(
    clk      => clk,
    rst      => rst,
    i_enable => sipo_enable,
    i_data   => sipo_data_in,
    o_data   => sipo_data_out
  );

  -- reg-state logic
  process(clk, rst)
  begin
    if rst = '1' then
      state_ll_reg <= s_idle;
      state_hl_reg <= s_idle;
      counter_reg  <= (others => '0');
      done_reg     <= '0';
      rw_reg       <= '0';
      data_reg     <= (others =>'0');
    elsif rising_edge(clk) then
      state_ll_reg <= state_ll_next;
      state_hl_reg <= state_hl_next;
      counter_reg  <= counter_next;
      done_reg     <= done_next;
      rw_reg       <= rw_next;
      data_reg     <= data_next;
    end if;
  end process;


  process(all)
  begin
    -- default
    state_hl_next <= state_hl_reg;
    data_next     <= data_reg;
    rw_next       <= rw_reg;

    -- Functional FSM
    case state_hl_reg is
      when s_idle =>
        if i_start = '1' then
          -- <TODO: enter s_command state>
          -- <TODO: register "i_data" into "data_reg" register>
          -- <TODO: register "i_rw" into "rw_reg" register>
        end if;

      when s_command =>
        if counter_reg = 7 and state_ll_reg = s_clk_high1 then
          -- <TODO: enter s_read or s_write states, depending on rw>
        end if;

      when s_write =>
        if counter_reg = 7 and state_ll_reg = s_clk_high1 then
          -- <TODO: enter s_idle state>
        end if;

      when s_read =>
        if counter_reg = 7 and state_ll_reg = s_clk_high1 then
          -- <TODO: enter s_idle state>
        end if;
    end case;
  end process;

  -- next-state logic
  process(all)
  begin
    -- default state
    state_ll_next <= state_ll_reg;

    -- default next-signal values (outputs, not registered, not a good style)
    done_next    <= '0';
    spi_cs       <= '1';
    spi_clk      <= '1';
    spi_sdi      <= piso_data_out;
    piso_load    <= '0';
    piso_enable  <= '0';
    sipo_enable  <= '0';
    piso_data_in <= (others => '0');
    sipo_data_in <= spi_sdo;
    counter_next <= counter_reg;

  --type state_act_t is (s_idle, s_start, s_clk_low0, s_clk_low1, s_clk_high0, s_clk_high1, s_stop);
    case state_ll_reg is
      when s_idle =>
        -- <TODO: if i_start is set, enter s_start state>
        -- <TODO: if i_start is set, set up piso_register with the command>
        -- <TODO: if i_start is set, zero counter>

      when s_start =>
        -- <TODO: enter s_clk_low0 state>
        -- <TODO: hold chip select low>

      when s_clk_low0 =>
        -- <TODO: enter s_clk_low1 state>
        -- <TODO: hold chip select low>
        -- <TODO: hold spi clock low>

      when s_clk_low1 =>
        -- <TODO: enter s_clk_high0 state>
        -- <TODO: hold chip select low>
        -- <TODO: hold spi clock low>

      when s_clk_high0 =>
        -- <TODO: enter s_clk_high1 state>
        -- <TODO: hold chip select low>
        -- <TODO: hold spi clock high>
        -- <TODO: if reading, enable sipo register>

      when s_clk_high1 =>
        -- <TODO: increment counter>
        -- <TODO: hold chip select low>
        -- <TODO: hold spi clock high>

        -- <TODO: enable piso register if in command or write states>

        -- <TODO: load piso register with input data if in command state and counter value is 7>

        -- <TODO: switch to s_stop state if in read/write states and counter value is 7,\
        -- switch to s_clk_low0>

      when s_stop =>
        -- <TODO: hold chip select high>
        -- <TODO: hold clock high>
        -- <TODO: set done_next signal high>
        -- <TODO: switch to idle state>
        
    end case;
  end process;

  -- outputs
  -- <TODO: connect output data>
  -- <TODO: connect output done register>

end architecture;
