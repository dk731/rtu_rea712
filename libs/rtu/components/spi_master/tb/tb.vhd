library std;
library ieee;
library rtu;
library osvvm;
library rtu_test;
library vunit_lib;

context vunit_lib.vunit_context;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.env.all;
use osvvm.RandomPkg.all;
use vunit_lib.com_pkg.all;
use rtu.data_types.all;

entity tb is
  generic(
    runner_cfg : string
  );
end entity;

architecture RTL of tb is
  -----------------------------------------------------------------------------
  -- Constants
  -----------------------------------------------------------------------------
  constant CLK_PERIOD : time := 10 ns;

  -----------------------------------------------------------------------------
  -- DUT interfacing
  -----------------------------------------------------------------------------
  signal clk     : std_logic := '0';
  signal rst     : std_logic := '0';
  signal i_start : std_logic := '0';
  signal i_rw    : std_logic := '0';
  signal i_addr  : std_logic_vector(5 downto 0) := (others => '0');
  signal i_data  : std_logic_vector(7 downto 0) := (others => '0');
  signal o_done  : std_logic := '0';
  signal o_data  : std_logic_vector(7 downto 0) := (others => '0');
  signal spi_cs  : std_logic := '0';
  signal spi_clk : std_logic := '0';
  signal spi_sdi : std_logic := '0';
  signal spi_sdo : std_logic := '0';

  -----------------------------------------------------------------------------
  -- DUT internal signals
  -----------------------------------------------------------------------------
  signal dut_state_hl_reg : state_hl_t;
  signal dut_state_ll_reg : state_ll_t;

begin
  -----------------------------------------------------------------------------
  -- clk
  -----------------------------------------------------------------------------
  clk <= not clk after CLK_PERIOD/2;

  -----------------------------------------------------------------------------
  -- DUT instantation
  -----------------------------------------------------------------------------
  DUT: entity rtu.spi_master
  port map(
    clk     => clk,
    rst     => rst,
    i_start => i_start,
    i_rw    => i_rw,
    i_addr  => i_addr,
    i_data  => i_data,
    o_done  => o_done,
    o_data  => o_data,
    spi_cs  => spi_cs,
    spi_clk => spi_clk,
    spi_sdi => spi_sdi,
    spi_sdo => spi_sdo
  );

  -----------------------------------------------------------------------------
  -- DUT internal signals
  -----------------------------------------------------------------------------
  dut_state_hl_reg <= << signal .tb.DUT.state_hl_reg : state_hl_t >>;
  dut_state_ll_reg <= << signal .tb.DUT.state_ll_reg : state_ll_t >>;

  -----------------------------------------------------------------------------
  -- Test sequencer
  -----------------------------------------------------------------------------
  process
    variable dut_addr : std_logic_vector(5 downto 0);
    variable dut_data : std_logic_vector(7 downto 0);
    variable dut_cmd  : std_logic_vector(7 downto 0);

    procedure start_transfer(
      rw : std_logic; 
      addr : std_logic_vector(5 downto 0);
      data : std_logic_vector(7 downto 0))
    is
    begin
      i_start <= '1';
      i_rw    <= rw;
      i_addr  <= addr;
      i_data  <= data;
      wait until rising_edge(clk);
      i_start <= '0';
      i_rw    <= '0';
      i_addr  <= (others => '0');
      i_data  <= (others => '0');
    end procedure;
  begin
    test_runner_setup(runner, runner_cfg);

    while test_suite loop
      if run("check_states.hl_write") then
        start_transfer('0', "000000", "00000000");

        wait until dut_state_hl_reg = s_command for 400 ns;
        check(dut_state_hl_reg = s_command, "Command state not reached");

        wait until dut_state_hl_reg = s_write for 400 ns;
        check(dut_state_hl_reg = s_write, "Write state not reached");

        wait until dut_state_hl_reg = s_idle for 400 ns;
        check(dut_state_hl_reg = s_idle, "Idle state not reached");


      elsif run("check_states.hl_read") then
        start_transfer('1', "000000", "00000000");

        wait until dut_state_hl_reg = s_command for 400 ns;
        check(dut_state_hl_reg = s_command, "Command state not reached");

        wait until dut_state_hl_reg = s_read for 400 ns;
        check(dut_state_hl_reg = s_read, "Read state not reached");

        wait until dut_state_hl_reg = s_idle for 400 ns;
        check(dut_state_hl_reg = s_idle, "Idle state not reached");


      elsif run("check_states.ll_write") then
        start_transfer('0', "000000", "00000000");

        wait until dut_state_ll_reg = s_start for 20 ns;
        check(dut_state_ll_reg = s_start, "s_start state not reached");

        for i in 0 to 15 loop
          wait until dut_state_ll_reg = s_clk_low0 for 20 ns;
          check(dut_state_ll_reg = s_clk_low0, "s_clk_low0 state not reached");

          wait until dut_state_ll_reg = s_clk_low1 for 20 ns;
          check(dut_state_ll_reg = s_clk_low1, "s_clk_low1 state not reached");

          wait until dut_state_ll_reg = s_clk_high0 for 20 ns;
          check(dut_state_ll_reg = s_clk_high0, "s_clk_high0 state not reached");

          wait until dut_state_ll_reg = s_clk_high1 for 20 ns;
          check(dut_state_ll_reg = s_clk_high1, "s_clk_high1 state not reached");
        end loop;

        wait until dut_state_ll_reg = s_stop for 20 ns;
        check(dut_state_ll_reg = s_stop, "s_stop state not reached");

        wait until dut_state_hl_reg = s_idle for 20 ns;
        check(dut_state_ll_reg = s_idle, "Idle state not reached");


      elsif run("check_states.ll_read") then
        start_transfer('1', "000000", "00000000");

        wait until dut_state_ll_reg = s_start for 20 ns;
        check(dut_state_ll_reg = s_start, "s_start state not reached");

        for i in 0 to 15 loop
          wait until dut_state_ll_reg = s_clk_low0 for 20 ns;
          check(dut_state_ll_reg = s_clk_low0, "s_clk_low0 state not reached");

          wait until dut_state_ll_reg = s_clk_low1 for 20 ns;
          check(dut_state_ll_reg = s_clk_low1, "s_clk_low1 state not reached");

          wait until dut_state_ll_reg = s_clk_high0 for 20 ns;
          check(dut_state_ll_reg = s_clk_high0, "s_clk_high0 state not reached");

          wait until dut_state_ll_reg = s_clk_high1 for 20 ns;
          check(dut_state_ll_reg = s_clk_high1, "s_clk_high1 state not reached");
        end loop;

        wait until dut_state_ll_reg = s_stop for 20 ns;
        check(dut_state_ll_reg = s_stop, "s_stop state not reached");

        wait until dut_state_hl_reg = s_idle for 20 ns;
        check(dut_state_ll_reg = s_idle, "Idle state not reached");


      elsif run("check_spi.write") then
        dut_addr := "111000";
        dut_data := "10101010";
        dut_cmd  := "00" & dut_addr;

        check(spi_cs  = '1', "spi_cs is not high before transmission");
        check(spi_clk = '1', "spi_clk is not high before transmission");

        start_transfer('0', dut_addr, dut_data);

        wait until spi_cs = '0' for 20 ns;
        check(spi_cs  = '0', "spi_cs not meeting start condition");
        check(spi_clk = '1', "spi_clk not high during the start condition");

        -- write command
        for i in 7 downto 0 loop
          wait until spi_clk = '0' for 21 ns;
          check(spi_clk = '0', "spi_clk not low during command transaction");
          check(spi_cs  = '0', "spi_cs not low during command transaction");

          wait until spi_clk = '1' for 21 ns;
          check(spi_clk = '1', "spi_clk not high during command transaction");
          check(spi_cs  = '0', "spi_cs not low during command transaction");
          check(spi_sdi = dut_cmd(i), "spi_sdi data (command transaction)"
            & " EXPECTED: " & to_string(dut_cmd(i))
            & " GOT: " & to_string(spi_sdi));
        end loop;

        -- write data
        for i in 7 downto 0 loop
          wait until spi_clk = '0' for 21 ns;
          check(spi_clk = '0', "spi_clk not low during data transaction");
          check(spi_cs  = '0', "spi_cs not low during data transaction");

          wait until spi_clk = '1' for 21 ns;
          check(spi_clk = '1', "spi_clk not high during data transaction");
          check(spi_cs  = '0', "spi_cs not low during data transaction");
          check(spi_sdi = dut_data(i), "spi_sdi data (data transaction)"
            & " EXPECTED: " & to_string(dut_data(i))
            & " GOT: " & to_string(spi_sdi));
        end loop;

        wait until spi_cs = '1' for 40 ns;
        check(spi_cs = '1', "spi_cs not meeting stop condition");

        wait until o_done = '1' for 20 ns;
        check(o_done = '1', "done signal not asserted");


      elsif run("check_spi.read") then
        dut_addr := "111000";
        dut_data := "10101010";
        dut_cmd  := "10" & dut_addr;

        check(spi_cs  = '1', "spi_cs is not high before transmission");
        check(spi_clk = '1', "spi_clk is not high before transmission");

        start_transfer('1', dut_addr, dut_data);

        wait until spi_cs = '0' for 20 ns;
        check(spi_cs  = '0', "spi_cs not meeting start condition");
        check(spi_clk = '1', "spi_clk not high during the start condition");

        -- write command
        for i in 7 downto 0 loop
          wait until spi_clk = '0' for 21 ns;
          check(spi_clk = '0', "spi_clk not low during command transaction");
          check(spi_cs  = '0', "spi_cs not low during command transaction");

          wait until spi_clk = '1' for 21 ns;
          check(spi_clk = '1', "spi_clk not high during command transaction");
          check(spi_cs  = '0', "spi_cs not low during command transaction");
          check(spi_sdi = dut_cmd(i), "spi_sdi data (command transaction)"
            & " EXPECTED: " & to_string(dut_cmd(i))
            & " GOT: " & to_string(spi_sdi));
        end loop;

        -- read data
        for i in 7 downto 0 loop
          wait until spi_clk = '0' for 21 ns;
          check(spi_clk = '0', "spi_clk not low during data transaction");
          check(spi_cs  = '0', "spi_cs not low during data transaction");
          spi_sdo <= dut_data(i);

          wait until spi_clk = '1' for 21 ns;
          check(spi_clk = '1', "spi_clk not high during data transaction");
          check(spi_cs  = '0', "spi_cs not low during data transaction");
        end loop;

        wait until spi_cs = '1' for 40 ns;
        check(spi_cs = '1', "spi_cs not meeting stop condition");

        wait until o_done = '1' for 20 ns;
        check(o_done = '1', "done signal not asserted");
        check(o_data = dut_data, "read data (data transaction)"
          & " EXPECTED: " & to_hstring(dut_data)
          & " GOT: "      & to_hstring(o_data));
      end if;
    end loop;

    test_runner_cleanup(runner);
  end process;
end architecture;
