library ieee;
library rtu;
use ieee.std_logic_1164.all;

--! @brief Custom ripple carry adder, adds two logic vectors that are
--!        interpreted as unsigned data type. Note, that output has an
--!        additional bit.
--!
entity ripple_carry_adder is
  generic(
    DATA_WIDTH : natural := 8
  );
  port(
    a : in  std_logic_vector(DATA_WIDTH-1 downto 0); --! input vector 
    b : in  std_logic_vector(DATA_WIDTH-1 downto 0); --! input vector
    q : out std_logic_vector(DATA_WIDTH   downto 0)  --! output vector
  );
end entity;

architecture RTL of ripple_carry_adder is
begin
end architecture;
