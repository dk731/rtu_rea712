library std;
library ieee;
library rtu;
library osvvm;
library rtu_test;
library vunit_lib;

context vunit_lib.vunit_context;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.env.all;
use osvvm.RandomPkg.all;
use vunit_lib.com_pkg.all;
use rtu_test.procedures.all;

entity tb is
  generic(
    runner_cfg            : string;
    DATA_WIDTH            : natural := 4;
    RANDOMIZED_TEST_COUNT : natural := 100
  );
end entity;

architecture RTL of tb is
  -----------------------------------------------------------------------------
  -- DUT interfacing
  -----------------------------------------------------------------------------
  signal a, b : std_logic_vector(DATA_WIDTH-1 downto 0);
  signal q    : std_logic_vector(DATA_WIDTH   downto 0);

begin
  -----------------------------------------------------------------------------
  -- DUT instantation
  -----------------------------------------------------------------------------
  DUT: entity rtu.ripple_carry_adder
  generic map(DATA_WIDTH => DATA_WIDTH)
  port map(
    a => a,
    b => b,
    q => q
  );

  -----------------------------------------------------------------------------
  -- Test sequencer
  -----------------------------------------------------------------------------
  process
    variable random   : RandomPType;
    variable expected : std_logic_vector(DATA_WIDTH downto 0);
    variable a_var, b_var : std_logic_vector(DATA_WIDTH-1 downto 0);
  begin
    test_runner_setup(runner, runner_cfg);
    random.InitSeed(random'instance_name);

    while test_suite loop
      if run("full_coverage") then
        for i in 0 to 2**DATA_WIDTH-1 loop
        for j in 0 to 2**DATA_WIDTH-1 loop
          expected := std_logic_vector(to_unsigned(i+j, DATA_WIDTH+1));

          a <= std_logic_vector(to_unsigned(i, DATA_WIDTH));
          b <= std_logic_vector(to_unsigned(j, DATA_WIDTH));
          wait for 10 ns;

          check_slv(q, expected, "Testing output of the adder");
        end loop;
        end loop;

      elsif run("randomized") then
        for i in 0 to RANDOMIZED_TEST_COUNT-1 loop
          a_var := random.RandSlv(a'length);
          b_var := random.RandSlv(b'length);
          expected := std_logic_vector(unsigned("0"&a_var)+unsigned("0"&b_var));

          a <= a_var;
          b <= b_var;
          wait for 10 ns;

          check_slv(q, expected, "Testing output of the adder");
        end loop;
      end if;
    end loop;

    test_runner_cleanup(runner);
  end process;
end architecture;
