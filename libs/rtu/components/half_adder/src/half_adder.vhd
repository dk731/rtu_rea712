--------------------------------------------------------------------------------
--! @file half_adder.vhd
--------------------------------------------------------------------------------

library ieee;
library rtu;
use ieee.std_logic_1164.all;

--! @brief Custom half adder implementation, implements the following thruth
--!        table:
--!
--!        |    Outputs  ||  Inputs   |
--!        ----------------------------
--!        | c_out |  q  ||  a  |  b  |
--!        |   0   |  0  ||  0  |  0  |
--!        |   0   |  1  ||  0  |  1  |
--!        |   0   |  1  ||  1  |  0  |
--!        |   1   |  0  ||  1  |  1  |
--!
entity half_adder is
  port(
    a     : in  std_logic; --! input bit
    b     : in  std_logic; --! input bit
    q     : out std_logic; --! sum bit
    c_out : out std_logic  --! carry out bit
  );
end entity;

architecture RTL of half_adder is
begin
end architecture;
