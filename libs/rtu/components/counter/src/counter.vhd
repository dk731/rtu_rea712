library ieee;
library rtu;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use rtu.functions.all;

entity counter is
  generic(
    COUNTER_MAX_VALUE : natural := 800
  );
  port(
    clk       : in std_logic;
    en        : in std_logic;
    o_counter : out unsigned(log2c(COUNTER_MAX_VALUE+1)-1 downto 0)
  );
end entity;

architecture RTL of counter is
  -- <your code goes here>
begin

  -- reg-state logic
  -- <your code goes here>

  -- next-state logic
  -- <your code goes here>

  -- output
  -- <your code goes here>

end architecture;
