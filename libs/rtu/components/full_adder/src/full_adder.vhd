library ieee;
library rtu;
use ieee.std_logic_1164.all;

--! @brief Custom full adder implementation, implements the following thruth
--!        table:
--!
--!        |    Outputs  ||      Inputs      |
--!        -----------------------------------
--!        | c_out |  q  || c_in |  a  |  b  |
--!        |   0   |  0  ||   0  |  0  |  0  |
--!        |   0   |  1  ||   0  |  0  |  1  |
--!        |   0   |  1  ||   0  |  1  |  0  |
--!        |   1   |  0  ||   0  |  1  |  1  |
--!        |   0   |  1  ||   1  |  0  |  0  |
--!        |   1   |  0  ||   1  |  0  |  1  |
--!        |   1   |  0  ||   1  |  1  |  0  |
--!        |   1   |  1  ||   1  |  1  |  1  |
--!
entity full_adder is
  port(
    a     : in  std_logic; --! input bit
    b     : in  std_logic; --! input bit
    c_in  : in  std_logic; --! carry in bit
    q     : out std_logic; --! sum bit
    c_out : out std_logic  --! carry out bit
  );
end entity;

architecture RTL of full_adder is
begin
end architecture;
