library std;
library ieee;
library rtu;
library rtu_test;
library vunit_lib;

context vunit_lib.vunit_context;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.env.all;
use vunit_lib.com_pkg.all;
use rtu_test.procedures.all;

entity tb is
  generic(
    runner_cfg : string
  );
end entity;

architecture RTL of tb is
  -----------------------------------------------------------------------------
  -- DUT interfacing
  -----------------------------------------------------------------------------
  signal a,b,c_in : std_logic;
  signal q, c_out : std_logic;

  -- custom type for storing checks
  type check_t is record
    c_out : std_logic;
    q     : std_logic;
    c_in  : std_logic;
    b     : std_logic;
    a     : std_logic;
  end record;
  type acheck_t is array(natural range<>) of check_t;

  -- truth table
  constant TESTS : acheck_t := (
    ('0','0','0','0','0'),
    ('0','1','0','0','1'),
    ('0','1','0','1','0'),
    ('1','0','0','1','1'),
    ('0','1','1','0','0'),
    ('1','0','1','0','1'),
    ('1','0','1','1','0'),
    ('1','1','1','1','1'));

begin
  -----------------------------------------------------------------------------
  -- DUT instantation
  -----------------------------------------------------------------------------
  DUT: entity rtu.full_adder
  port map(
    a     => a,
    b     => b,
    c_in  => c_in,
    q     => q,
    c_out => c_out);

  -----------------------------------------------------------------------------
  -- Test sequencer
  -----------------------------------------------------------------------------
  process
  begin
    test_runner_setup(runner, runner_cfg);

    while test_suite loop
      if run("full_coverage") then
        for i in TESTS'range loop
          a    <= TESTS(i).a;
          b    <= TESTS(i).b;
          c_in <= TESTS(i).c_in;
          wait for 10 ns;
          check_sl(q,     TESTS(i).q,     "Checking output bit");
          check_sl(c_out, TESTS(i).c_out, "Checking carry out bit");
        end loop;
      end if;
    end loop;

    test_runner_cleanup(runner);
  end process;
end architecture;
