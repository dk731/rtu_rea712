--------------------------------------------------------------------------------
--! @file procedures.vhd
--------------------------------------------------------------------------------

-- libraries and packages
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- declarations of the package (types, prototypes of functions and procedures)
package procedures is
end package;

-- implementations of the package (functions, procedures)
package body procedures is
end package body;
